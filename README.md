# 安装

```sh
composer require wjy/locker
```

# 使用

首先需要在程序内初始化`redis`

```php
/**
     * @var RedisLock
     */
    protected $redis;

    public function __construct(RedisFactory $redisFactory)
    {
        $this->redis = $redisFactory->get('default');
    }
```

## 读写锁

```php
    public function lockA(ResponseInterface $response)
    {
        // 初始化RedisLock 参数:redis实例 锁名称 超时时间
        $lock = new RedisLock($this->redis, 'lock', 4);
        // 读锁
        $res = $lock->readLock(4, function () {
            return [456];
        });
        // 写锁
        $res = $lock->writeLock(4, function() {
            return [456];
        })
        return $response->json(['res' => $res]);
    }
```

