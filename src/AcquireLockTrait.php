<?php

namespace Wjy\Locker;

trait AcquireLockTrait
{
    protected function acquireShareLock(): bool
    {
        $shareLockScript = LockScripts::shareLock();
        $expireTime = $this->seconds > 0 ? $this->seconds : 30;

        $result = $this->redis->eval($shareLockScript, [$this->getReadWriteLockKey(), self::LOCK_MODE_SHARE, $expireTime], 1);
        return intval($result) === 1;
    }

    protected function releaseShareLock()
    {
        $lua = LockScripts::releaseShareLock();
        $this->redis->eval($lua, [$this->getReadWriteLockKey(), self::LOCK_MODE_SHARE], 1);
    }

    protected function acquireWriteLock(): bool
    {
        $lua = LockScripts::writeLock();
        $expireTime = $this->seconds > 0 ? $this->seconds : 30;

        $result = $this->redis->eval($lua, [$this->getReadWriteLockKey(), self::LOCK_MODE_WRITE, $this->owner, $expireTime], 1);
        return intval($result) === 1;
    }

    protected function releaseWriteLock(): void
    {
        $lua = LockScripts::releaseWriteLock();
        $this->redis->eval($lua, [$this->getReadWriteLockKey(), self::LOCK_MODE_WRITE, $this->owner], 1);
    }

    protected function getReadWriteLockKey(): string
    {
        return 'readWriteLock:' . $this->name;
    }

    protected function addLockPeriod($ttl = 30): void
    {
        var_dump('30s added');
        $this->redis->expire($this->getReadWriteLockKey(), $ttl);
    }
}