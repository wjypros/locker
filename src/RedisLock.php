<?php

namespace Wjy\Locker;

use Lysice\HyperfRedisLock\LockTimeoutException;
use Swoole\Timer;

class RedisLock extends \Lysice\HyperfRedisLock\RedisLock
{
    use AcquireLockTrait;

    const LOCK_MODE_SHARE = 1;
    const LOCK_MODE_WRITE = 2;

    /**
     * @param $seconds
     * @param null $callback
     * @return bool|mixed
     * @throws LockTimeoutException
     */
    public function readLock($seconds, $callback = null)
    {
        $starting = $this->currentTime();
        while(! $this->acquireShareLock()) {
            usleep(rand(10000, 250 * 1000));
            if($this->currentTime() - $seconds >= $starting) {
                throw new LockTimeoutException();
            }
        }

        if(is_callable($callback)) {
            try {
                $timer = Timer::tick($this->seconds*750, function () {
                    $this->addLockPeriod($this->seconds);
                });
                return $callback();
            } finally {
                Timer::clear($timer);
                $this->releaseShareLock();
            }
        }

        return true;
    }

    /**
     * @param $seconds
     * @param null $callback
     * @return bool|mixed
     * @throws LockTimeoutException
     */
    public function writeLock($seconds, $callback = null)
    {
        $starting = $this->currentTime();
        while(! $this->acquireWriteLock()) {
            usleep(rand(10000, 250 * 1000));
            if($this->currentTime() - $seconds >= $starting) {
                throw new LockTimeoutException();
            }
        }

        if(is_callable($callback)) {
            try {
                $timer = Timer::tick($this->seconds*750, function () {
                    $this->addLockPeriod($this->seconds);
                });
                return $callback();
            } finally {
                Timer::clear($timer);
                $this->releaseWriteLock();
            }
        }

        return true;
    }
}